# SkunkShims.js

## Synopsis

ES5 & ES6 javascript shims for Skunkworks. 

## Functionality
- Adds some ES5 and ES6 functionality to Skunkwork's ES3 JScript. 


## Example usage.

```
	// Load the scripts via your swx file.
    <script src="modules\\SkunkShims\\es5-shim.js"/>
	<script src="modules\\SkunkShims\\es6-shim.js"/>
    // Or via require().
    require("SkunkShims/es5-shim.js");
	require("SkunkShims/es6-shim.js");
```

## License
MIT License	(http://opensource.org/licenses/MIT)